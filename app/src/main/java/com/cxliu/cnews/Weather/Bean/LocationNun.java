package com.cxliu.cnews.Weather.Bean;

import java.io.Serializable;

/**
 * Created by liuchenxu on 2016/8/2.
 */
public class LocationNun implements Serializable {

    private double lng;

    private double lat;

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
