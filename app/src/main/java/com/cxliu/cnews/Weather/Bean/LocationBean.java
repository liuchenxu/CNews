package com.cxliu.cnews.Weather.Bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/8/2.
 */
public class LocationBean implements Serializable {
    private String status;
    private LocationResult result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocationResult getResult() {
        return result;
    }

    public void setResult(LocationResult result) {
        this.result = result;
    }
}
