package com.cxliu.cnews.Weather.Widget;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cxliu.cnews.R;
import com.cxliu.cnews.Weather.Bean.WeatherBean;
import com.cxliu.cnews.Weather.Presenter.WeatherImpl;
import com.cxliu.cnews.Weather.Presenter.WeatherPresenter;
import com.cxliu.cnews.Weather.View.IWeatherView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liuchenxu on 2016/8/2.
 */
public class WeatherFragment extends Fragment implements IWeatherView {

    private WeatherPresenter presenter;

    private TextView mTodayTV;
    private ImageView mTodayWeatherImage;
    private TextView mTodayTemperatureTV;
    private TextView mTodayWindTV;
    private TextView mTodayWeatherTV;
    private TextView mCityTV;
    private ProgressBar mProgressBar;
    private LinearLayout mWeatherLayout;
    private LinearLayout mWeatherContentLayout;
    private FrameLayout mRootLayout;

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void loadWeatherInfo(List<WeatherBean> beans) {
        hideProgress();
        List<View> adapterList = new ArrayList<View>();
        WeatherBean bean=beans.remove(0);
        for (WeatherBean weatherBean : beans) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_weather, null, false);
            TextView dateTV = (TextView) view.findViewById(R.id.date);
            ImageView todayWeatherImage = (ImageView) view.findViewById(R.id.weatherImage);
            TextView todayTemperatureTV = (TextView) view.findViewById(R.id.weatherTemp);
            TextView todayWindTV = (TextView) view.findViewById(R.id.wind);
            TextView todayWeatherTV = (TextView) view.findViewById(R.id.weather);

            dateTV.setText(weatherBean.getWeek());
            todayTemperatureTV.setText(weatherBean.getTemperature());
            todayWindTV.setText(weatherBean.getWind());
            todayWeatherTV.setText(weatherBean.getWeather());
            todayWeatherImage.setImageResource(weatherBean.getImageRes());
            mWeatherContentLayout.addView(view);
            adapterList.add(view);
        }
        mTodayTV.setText(bean.getDate());
        mTodayTemperatureTV.setText(bean.getTemperature());
        mTodayWeatherTV.setText(bean.getWeather());
        mTodayWindTV.setText(bean.getWind());
        mTodayWeatherImage.setImageResource(bean.getImageRes());
        mWeatherLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void getLocationFail() {
        hideProgress();
        Toast.makeText(getActivity(), "获取地理位置失败", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getWeatherInfoFail() {
        hideProgress();
        Toast.makeText(getActivity(), "获取天气信息失败", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setCity(String cityName) {
        mCityTV.setText(cityName);
    }

    public static WeatherFragment newInstance() {
        WeatherFragment fragment = new WeatherFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter=new WeatherImpl(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        mTodayTV = (TextView) view.findViewById(R.id.today);
        mTodayWeatherImage = (ImageView) view.findViewById(R.id.weatherImage);
        mTodayTemperatureTV = (TextView) view.findViewById(R.id.weatherTemp);
        mTodayWindTV = (TextView) view.findViewById(R.id.wind);
        mTodayWeatherTV = (TextView) view.findViewById(R.id.weather);
        mCityTV = (TextView)view.findViewById(R.id.city);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress);
        mWeatherLayout = (LinearLayout) view.findViewById(R.id.weather_layout);
        mWeatherContentLayout = (LinearLayout) view.findViewById(R.id.weather_content);
        mRootLayout = (FrameLayout) view.findViewById(R.id.root_layout);
        presenter.getLocationInfo(getActivity());
        return view;
    }
}
