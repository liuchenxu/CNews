package com.cxliu.cnews.Weather.Presenter;

import android.content.Context;

/**
 * Created by liuchenxu on 2016/8/2.
 */
public interface WeatherPresenter {
    void getLocationInfo(Context context);
    void getWeatherInfo(String cityname);
}
