package com.cxliu.cnews.Weather.View;

import com.cxliu.cnews.Weather.Bean.WeatherBean;

import java.util.List;

/**
 * Created by liuchenxu on 2016/8/2.
 */
public interface IWeatherView {
//    void getLocation();
    void showProgress();
    void hideProgress();
    void loadWeatherInfo(List<WeatherBean> beans);
    void getLocationFail();
    void getWeatherInfoFail();
    void setCity(String cityName);
}
