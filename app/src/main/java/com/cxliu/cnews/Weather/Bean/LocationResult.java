package com.cxliu.cnews.Weather.Bean;

import java.io.Serializable;

/**
 * Created by liuchenxu on 2016/8/2.
 */
public class LocationResult implements Serializable {

    private LocationNun location;

    private String formatted_address;

    private String business;

    private AddressComponent addressComponent;

    private int cityCode;

    public LocationNun getLocation() {
        return location;
    }

    public void setLocation(LocationNun location) {
        this.location = location;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public AddressComponent getAddressComponent() {
        return addressComponent;
    }

    public void setAddressComponent(AddressComponent addressComponent) {
        this.addressComponent = addressComponent;
    }

    public int getCityCode() {
        return cityCode;
    }

    public void setCityCode(int cityCode) {
        this.cityCode = cityCode;
    }
}
