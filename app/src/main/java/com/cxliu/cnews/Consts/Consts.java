package com.cxliu.cnews.Consts;

/**
 * Created by lcx on 2016/7/11.
 */

public class Consts {
    public static final int PAGE_SIZE=20;
    public static final String HOST="http://c.m.163.com/";
    public static final String TOP_NEWS="nc/article/headline/T1348647909107/";
    public static final String END_HTML=".html";
    public static final String TYPE_TOP_NEWS_ID="T1348647909107";
    public static final String TYPE_NBA_NEWS_ID="T1348649145984";
    public static final String TYPE_CAR_NEWS_ID="T1348654060988";
    public static final String TYPE_JOKE_NEWS_ID="T1350383429665";
    public static final String NEWS_TYPE_TOP_NEWS="0";
    public static final String NEWS_TYPE_NBA_NEWS="1";
    public static final String NEWS_TYPE_CAR_NEWS="2";
    public static final String NEWS_TYPE_JOKES_NEWS="3";
}
