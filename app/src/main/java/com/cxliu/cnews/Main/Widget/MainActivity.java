package com.cxliu.cnews.Main.Widget;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.cxliu.cnews.About.AboutFragment;
import com.cxliu.cnews.Images.widget.ImageListFragment;
import com.cxliu.cnews.Main.Presenter.MainPresenter;
import com.cxliu.cnews.Main.Presenter.MainPresenterImp;
import com.cxliu.cnews.Main.View.IMainView;
import com.cxliu.cnews.News.Widget.NewsFragment;
import com.cxliu.cnews.R;
import com.cxliu.cnews.Weather.Widget.WeatherFragment;

import static android.R.attr.id;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,IMainView {

    private MainPresenter mainPresenterImp;
    Toolbar toolbar;
    NavigationView navigationView;
    DrawerLayout drawer;
    AppBarLayout main_appbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        main_appbar=(AppBarLayout)findViewById(R.id.main_appbar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("新闻");
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mainPresenterImp=new MainPresenterImp(this);
        switch2Fragment(R.id.nav_news);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        mainPresenterImp.switchFragment(id);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void switch2Fragment(int id) {
        navigationView.setSelected(true);
        if (id == R.id.nav_news) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, NewsFragment.newInstance(this)).commit();
            toolbar.setTitle("新闻");
        } else if (id == R.id.nav_pics) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, ImageListFragment.newInstance()).commit();
            toolbar.setTitle("图片");
        } else if (id == R.id.nav_weather) {
            toolbar.setTitle("天气");
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, WeatherFragment.newInstance()).commit();
        } else if (id == R.id.nav_about) {
            toolbar.setTitle("关于");
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, AboutFragment.newInstance()).commit();
        }
    }


    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        mainPresenterImp.switchFragment(id);
                        menuItem.setChecked(true);
                        drawer.closeDrawers();
                        return true;
                    }
                });
    }
}
