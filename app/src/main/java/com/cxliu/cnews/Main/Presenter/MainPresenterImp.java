package com.cxliu.cnews.Main.Presenter;

import com.cxliu.cnews.Main.View.IMainView;

/**
 *
 * Created by lcx on 2016/7/8.
 */

public class MainPresenterImp implements MainPresenter {

    private IMainView mIMainView;

    public MainPresenterImp(IMainView mainview) {
        mIMainView = mainview;
    }

    @Override
    public void switchFragment(int id) {
        mIMainView.switch2Fragment(id);
    }
}
