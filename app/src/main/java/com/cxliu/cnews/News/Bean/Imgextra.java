package com.cxliu.cnews.News.Bean;

import java.io.Serializable;

/**
 * Created by lcx on 2016/7/11.
 */

public class Imgextra implements Serializable {

    private String imgsrc;

    public String getImgsrc() {
        return imgsrc;
    }

    public void setImgsrc(String imgsrc) {
        this.imgsrc = imgsrc;
    }
}
