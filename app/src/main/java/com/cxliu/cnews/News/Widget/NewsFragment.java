package com.cxliu.cnews.News.Widget;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cxliu.cnews.Consts.Consts;
import com.cxliu.cnews.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewsFragment extends Fragment {

    public Context mContext;
    private static NewsFragment instance;
    @Bind(R.id.news_tablayout)
    TabLayout news_tablayout;
    @Bind(R.id.news_viewpager)
    ViewPager news_viewpager;
    public NewsFragment() {
    }

    public static NewsFragment newInstance(Context context) {
        if (instance == null) {
            instance = new NewsFragment();
            instance.mContext = context;
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        ButterKnife.bind(view);
        news_tablayout=(TabLayout)view.findViewById(R.id.news_tablayout);
        news_viewpager=(ViewPager)view.findViewById(R.id.news_viewpager);
        setViewPager();
        news_tablayout.setupWithViewPager(news_viewpager);
        setTablayout();
        return view;
    }

    private void setTablayout(){
        news_tablayout.getTabAt(0).setText("头条");
        news_tablayout.getTabAt(1).setText("NBA");
        news_tablayout.getTabAt(2).setText("汽车");
        news_tablayout.getTabAt(3).setText("笑话");
    }

    private void setViewPager(){
        MyAdapter myAdapter=new MyAdapter(getChildFragmentManager());
        //头条的Fragment
        NewsListFragment mNewsListFragment=NewsListFragment.newInstance(Consts.NEWS_TYPE_TOP_NEWS);
        //NBA的Fragment
        NewsListFragment mNewsListNBAFragment=NewsListFragment.newInstance(Consts.NEWS_TYPE_NBA_NEWS);
        //汽车的Fragment
        NewsListFragment mNewsListCarFragment=NewsListFragment.newInstance(Consts.NEWS_TYPE_CAR_NEWS);

        //笑话
        NewsListFragment mNewsListJokeFragment=NewsListFragment.newInstance(Consts.NEWS_TYPE_JOKES_NEWS);
        myAdapter.setFragments(Consts.NEWS_TYPE_TOP_NEWS,mNewsListFragment);
        myAdapter.setFragments(Consts.NEWS_TYPE_NBA_NEWS,mNewsListNBAFragment);
        myAdapter.setFragments(Consts.NEWS_TYPE_CAR_NEWS,mNewsListCarFragment);
        myAdapter.setFragments(Consts.NEWS_TYPE_JOKES_NEWS,mNewsListJokeFragment);
        news_viewpager.setAdapter(myAdapter);
        news_viewpager.setOffscreenPageLimit(3);
    }

    class MyAdapter extends FragmentPagerAdapter{

        private List<Fragment> fragmentList=new ArrayList<Fragment>();

        private List<String> tytpeList=new ArrayList<String>();

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        public void setFragments(String type,Fragment fragment){
            fragmentList.add(fragment);
            tytpeList.add(type);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }

}
