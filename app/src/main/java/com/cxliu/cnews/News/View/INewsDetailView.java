package com.cxliu.cnews.News.View;

/**
 * 新闻详情接口
 * Created by lcx on 2016/7/15.
 */

public interface INewsDetailView {
    void shoeProgress();
    void hideProgress();
    void showNewsDetail(String newsDetail);
    void getNewsDetail(String docId);
    void showFail();
}
