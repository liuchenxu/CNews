package com.cxliu.cnews.News.Api;

import android.util.Log;

import com.cxliu.cnews.News.Bean.SimpleNewsBean;
import com.cxliu.cnews.News.Bean.SimpleNewsEntity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


/**
 * Created by lcx on 2016/7/11.
 */

public class ApiManager {
    public static ApiService apiManager;

    public static ApiService getApi() {
        if (apiManager == null) {
            intAPI();
        }
        return apiManager;
    }

    public static void intAPI() {
        Retrofit mRetrofit = new Retrofit.Builder()
                .baseUrl("http://c.m.163.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create()).build();
        apiManager = mRetrofit.create(ApiService.class);
    }

    public synchronized static void getCallNewsList(String pageIndex, final String newsType, final NewsApiInterface callback) {
        getApi().getCallNewsList(pageIndex, newsType).enqueue(new Callback<SimpleNewsEntity>() {
            @Override
            public void onResponse(Call<SimpleNewsEntity> call, retrofit2.Response<SimpleNewsEntity> response) {
                if (response.isSuccess()) {
                    if (response.errorBody() != null) {
                        callback.onFail();
                    } else {
                        if (newsType.equals("T1348647909107")) {
                            callback.onSuccess(response.body().getT1348647909107());
                        } else if (newsType.equals("T1350383429665")) {
                            callback.onSuccess(response.body().getT1350383429665());
                        } else if (newsType.equals("T1348649145984")) {
                            callback.onSuccess(response.body().getT1348649145984());
                        } else if (newsType.equals("T1348654060988")) {
                            callback.onSuccess(response.body().getT1348654060988());
                        }
                    }
                } else {
                    callback.onFail();
                }
            }

            @Override
            public void onFailure(Call<SimpleNewsEntity> call, Throwable t) {
                callback.onFail();
                if (t != null) {
                    Log.i("error", t.toString());
                }
            }
        });
    }

    /**
     * 获取信息详情
     *
     * @param docid
     */
    public static void getNewsDetail(final String docid, final NewsDeatilApiInterface call) {
        getApi().getNewsDetail(docid).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).unsubscribeOn(Schedulers.io())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody response) {
                        if(response!=null){
                            try {
                                String jsonObjectString = response.string();
                                JSONObject obj=new JSONObject(jsonObjectString);
//                                Log.i("getNewsDetail",obj.getString(docid));
                                call.onSuccess(new JSONObject(obj.getString(docid)).getString("body"));
                            } catch (IOException e) {
                                e.printStackTrace();
                                call.onFail();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                call.onFail();
                            }
                        }
                        else{
                            call.onFail();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        call.onFail();
                    }
                });
    }

    public interface ApiService {
        @Headers("User-Agent:Mozilla/5.0")
        @POST("nc/article/headline/" + "{type}/" + "{pageIndex}" + "-20.html")
        Call<SimpleNewsEntity> getCallNewsList(@Path("pageIndex") String pageIndex, @Path("type") String type);

        @Headers("User-Agent:Mozilla/5.0")
        @POST("nc/article/" + "{docid}" + "/full.html")
        Observable<ResponseBody> getNewsDetail(@Path("docid") String docid);

    }


    public interface NewsApiInterface {

        public void onSuccess(List<SimpleNewsBean> beans);

        public void onFail();
    }

    public interface NewsDeatilApiInterface {

        public void onSuccess(String beans);

        public void onFail();
    }
}
