package com.cxliu.cnews.News.Widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cxliu.cnews.News.Bean.SimpleNewsBean;
import com.cxliu.cnews.R;
import com.cxliu.cnews.Utils.ImageDisplayUtils;
import com.cxliu.cnews.Utils.RecycleViewViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by a on 2016/7/12.
 */

public class NewsRecyclerListAdapter extends RecyclerView.Adapter<RecycleViewViewHolder> {

    private List<SimpleNewsBean> items = new ArrayList<SimpleNewsBean>();

    private Context mContext;

    private onItemClickListener clickListener;

    public NewsRecyclerListAdapter(Context context){
        mContext=context;
    }

    public void setLists(List<SimpleNewsBean> beans) {
        if (beans != null) {
            items.clear();
            items.addAll(beans);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecycleViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.adapter_news_list, parent, false);
        RecycleViewViewHolder holder = new RecycleViewViewHolder(view,clickListener,items);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecycleViewViewHolder holder, final int position) {
        ImageDisplayUtils.displayImage(mContext,items.get(position).getImgsrc(),holder.item_img);
        holder.item_title.setText(items.get(position).getTitle());
        holder.item_subtitle.setText(items.get(position).getDigest());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface onItemClickListener{
        public void onClick(SimpleNewsBean news);
    }

    public void setClickListener(onItemClickListener listener){
        clickListener=listener;
    }
}
