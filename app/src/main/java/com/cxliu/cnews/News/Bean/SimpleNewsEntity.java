package com.cxliu.cnews.News.Bean;

import java.util.List;

/**
 * Created by lcx on 2016/7/12.
 */

public class SimpleNewsEntity {

    /**
     * 新闻头条List
     */
    private List<SimpleNewsBean> T1348647909107 ;

    /**
     * NBA
     */
    private List<SimpleNewsBean> T1348649145984;

    /**
     * Car
     */
    private List<SimpleNewsBean> T1348654060988;

    private List<SimpleNewsBean> T1350383429665;

    public List<SimpleNewsBean> getT1348647909107() {
        return T1348647909107;
    }

    public void setT1348647909107(List<SimpleNewsBean> t1348647909107) {
        T1348647909107 = t1348647909107;
    }

    public List<SimpleNewsBean> getT1348649145984() {
        return T1348649145984;
    }

    public void setT1348649145984(List<SimpleNewsBean> t1348649145984) {
        T1348649145984 = t1348649145984;
    }

    public List<SimpleNewsBean> getT1348654060988() {
        return T1348654060988;
    }

    public void setT1348654060988(List<SimpleNewsBean> t1348654060988) {
        T1348654060988 = t1348654060988;
    }

    public List<SimpleNewsBean> getT1350383429665() {
        return T1350383429665;
    }

    public void setT1350383429665(List<SimpleNewsBean> t1350383429665) {
        T1350383429665 = t1350383429665;
    }
}
