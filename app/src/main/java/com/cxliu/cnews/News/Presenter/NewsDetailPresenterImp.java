package com.cxliu.cnews.News.Presenter;

import com.cxliu.cnews.News.Api.ApiManager;
import com.cxliu.cnews.News.View.INewsDetailView;

/**
 * Created by liucxu on 2016/7/17.
 */
public class NewsDetailPresenterImp implements NewsDetailPresenter {

    private INewsDetailView iNewsDetailView;


    @Override
    public void loadNewsDetail(String docid) {
        ApiManager.getNewsDetail(docid, new ApiManager.NewsDeatilApiInterface() {
            @Override
            public void onSuccess(String beans) {
                iNewsDetailView.showNewsDetail(beans);
            }

            @Override
            public void onFail() {
                iNewsDetailView.showFail();
            }
        });
    }

    public NewsDetailPresenterImp(INewsDetailView view) {
        iNewsDetailView = view;
    }

}
