package com.cxliu.cnews.News.Widget;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cxliu.cnews.Consts.Consts;
import com.cxliu.cnews.News.Bean.SimpleNewsBean;
import com.cxliu.cnews.News.Presenter.NewsPresenter;
import com.cxliu.cnews.News.Presenter.NewsPresenterImp;
import com.cxliu.cnews.News.View.INewsListView;
import com.cxliu.cnews.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lcx on 2016/7/11.
 */

public class NewsListFragment extends Fragment implements INewsListView,NewsRecyclerListAdapter.onItemClickListener {

    private SwipeRefreshLayout swipe_refresh_layout;
    //    private ListView news_list_view;
    private RecyclerView id_recyclerview;
    private String mType = "0";
    private List<SimpleNewsBean> newsList = new ArrayList<SimpleNewsBean>();
    //    private NewsListAdapter newsListAdapter=null;
    private NewsRecyclerListAdapter mNewsRecyclerListAdapter = null;
    private NewsPresenter mNewsPresenter;
    private int Page_Index = 0;
    private boolean isRefresh = true;

    public NewsListFragment() {
        mNewsPresenter = new NewsPresenterImp(this);
    }

    public void setType(String type) {
        this.mType = type;
    }

    public static NewsListFragment newInstance(String type) {
        Bundle args = new Bundle();
        NewsListFragment fragment = new NewsListFragment();
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNewsPresenter = new NewsPresenterImp(this);
        this.mType = getArguments().getString("type");
    }


    @Override
    public void showProgress() {
        swipe_refresh_layout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipe_refresh_layout.setRefreshing(false);
    }

    @Override
    public void loadNewsList(String type, int page_index) {
        if (Page_Index == 0) {
            showProgress();
        }
        mNewsPresenter.loadNews(type, page_index);
    }

    @Override
    public void setNewsList(List<SimpleNewsBean> news) {

        List<SimpleNewsBean> localNews = new ArrayList<SimpleNewsBean>();
       for (int i = 0; i < news.size(); i++) {
            if ((!TextUtils.isEmpty(news.get(i).getSkipType())) && (news.get(i).getSkipType().equals("special"))) {
                continue;
            }
           if ((!TextUtils.isEmpty(news.get(i).getTAGS()) ) && (TextUtils.isEmpty(news.get(i).getTAG()))) {
                continue;
            }
            if ((news.get(i).getImgextra() == null) || (news.get(i).getImgextra().size() == 0)) {
                localNews.add(news.get(i));
            }
        }

        hideProgress();
        if (isRefresh) {
            Page_Index = 0;
            newsList.clear();
            newsList.addAll(localNews);
        } else {
            Page_Index += Consts.PAGE_SIZE;
            newsList.addAll(localNews);
        }

        mNewsRecyclerListAdapter.setLists(newsList);
    }

    @Override
    public void loadDataFail() {
        Toast.makeText(getActivity(), "加载失败", Toast.LENGTH_SHORT).show();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_list, container, false);
        swipe_refresh_layout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
//        news_list_view = (ListView) view.findViewById(news_list_view);
        id_recyclerview = (RecyclerView) view.findViewById(R.id.id_recyclerview);
        //设置布局管理器
        id_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        //设置Item增加、移除动画
        id_recyclerview.setItemAnimator(new DefaultItemAnimator());
        swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isRefresh = true;
                Page_Index = 0;
                loadNewsList(NewsListFragment.this.mType, 0);
            }
        });
        loadNewsList(this.mType, Page_Index);
        mNewsRecyclerListAdapter = new NewsRecyclerListAdapter(getActivity());
        mNewsRecyclerListAdapter.setClickListener(new NewsRecyclerListAdapter.onItemClickListener() {
            @Override
            public void onClick(SimpleNewsBean news) {
                Intent intent=new Intent(getActivity(),NewsDetailActivity.class);
                intent.putExtra("bean",news);
                getActivity().startActivity(intent);
            }
        });
        id_recyclerview.setAdapter(mNewsRecyclerListAdapter);
        return view;
    }

    @Override
    public void onClick(SimpleNewsBean news) {
        Intent intent=new Intent(getActivity(),NewsDetailActivity.class);
        intent.putExtra("bean",news);
        getActivity().startActivity(intent);
    }
}
