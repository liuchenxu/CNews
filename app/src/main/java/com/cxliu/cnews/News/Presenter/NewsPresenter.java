package com.cxliu.cnews.News.Presenter;

/**
 * Created by lcx on 2016/7/11.
 */

public interface NewsPresenter {
    public void loadNews(String type,int page_index);
}
