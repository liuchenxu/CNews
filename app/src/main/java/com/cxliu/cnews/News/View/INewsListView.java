package com.cxliu.cnews.News.View;

import com.cxliu.cnews.News.Bean.SimpleNewsBean;

import java.util.List;

/**
 * 新闻列表Fragemnt接口
 * Created by a on 2016/7/11.
 */

public interface INewsListView {
    void showProgress();
    void hideProgress();
    void loadNewsList(String type,int page_index);
    void setNewsList(List<SimpleNewsBean> news);
    void loadDataFail();
}
