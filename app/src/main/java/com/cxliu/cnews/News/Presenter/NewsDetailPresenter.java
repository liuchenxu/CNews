package com.cxliu.cnews.News.Presenter;

/**
 * Created by liucxu on 2016/7/17.
 */
public interface NewsDetailPresenter {
    public void loadNewsDetail(String docid);
}
