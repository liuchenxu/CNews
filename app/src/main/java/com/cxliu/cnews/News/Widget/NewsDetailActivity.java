package com.cxliu.cnews.News.Widget;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cxliu.cnews.News.Bean.SimpleNewsBean;
import com.cxliu.cnews.News.Presenter.NewsDetailPresenter;
import com.cxliu.cnews.News.Presenter.NewsDetailPresenterImp;
import com.cxliu.cnews.News.View.INewsDetailView;
import com.cxliu.cnews.R;
import com.cxliu.cnews.Utils.ImageDisplayUtils;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.ButterKnife;

public class NewsDetailActivity extends AppCompatActivity implements INewsDetailView {

    private SimpleNewsBean bean;
    private AppBarLayout appbar;
    private Toolbar toolbar;
    private ProgressBar progress_bar;
    private HtmlTextView htmlText;
    private ImageView backdrop;
    private NewsDetailPresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        appbar = (AppBarLayout) findViewById(R.id.appbar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        progress_bar=(ProgressBar) findViewById(R.id.progress_bar);
        htmlText=(HtmlTextView)findViewById(R.id.htNewsContent);
        backdrop=(ImageView)findViewById(R.id.backdrop);
//        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        //显示左上角的返回按钮
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //点击左上角的返回按钮的事件响应
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewsDetailActivity.this.finish();
            }
        });
        Intent intent = getIntent();
        bean = (SimpleNewsBean) intent.getSerializableExtra("bean");
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(bean.getTitle());
        ImageDisplayUtils.displayImage(this,bean.getImgsrc(),backdrop);
        presenter=new NewsDetailPresenterImp(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getNewsDetail(bean.getDocid());
    }

    @Override
    public void shoeProgress() {
        progress_bar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress_bar.setVisibility(View.GONE);
    }

    @Override
    public void showNewsDetail(String newsDetail) {
        hideProgress();
        htmlText.setHtmlFromString(newsDetail, new HtmlTextView.LocalImageGetter());
    }

    @Override
    public void getNewsDetail(String docId) {
        shoeProgress();
        presenter.loadNewsDetail(bean.getDocid());
    }

    @Override
    public void showFail() {
        hideProgress();
        Toast.makeText(this, "加载失败", Toast.LENGTH_SHORT).show();
    }
}
