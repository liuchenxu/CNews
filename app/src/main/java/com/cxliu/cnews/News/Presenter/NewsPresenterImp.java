package com.cxliu.cnews.News.Presenter;

import com.cxliu.cnews.News.Api.ApiManager;
import com.cxliu.cnews.News.Bean.SimpleNewsBean;
import com.cxliu.cnews.News.View.INewsListView;

import java.util.List;

/**
 * Created by a on 2016/7/11.
 */

public class NewsPresenterImp implements NewsPresenter {
    @Override
    public void loadNews(String type, int page_index) {
        String typeString="";
        if(type.equals("0")){
            typeString="T1348647909107";
        }
        else if(type.equals("1")){
            typeString="T1348649145984";
        }
        else if(type.equals("2")){
            typeString="T1348654060988";
        }
        else if(type.equals("3")){
            typeString="T1350383429665";
        }
        ApiManager.getCallNewsList(page_index+"",typeString, new ApiManager.NewsApiInterface() {
            @Override
            public void onSuccess(List<SimpleNewsBean> beans) {
                iView.setNewsList(beans);
            }

            @Override
            public void onFail() {
                iView.loadDataFail();
            }
        });
    }

    private INewsListView iView;

//    private NewsModel newsModel;

    public NewsPresenterImp(INewsListView iNewsListView){
        iView=iNewsListView;
    }
}
