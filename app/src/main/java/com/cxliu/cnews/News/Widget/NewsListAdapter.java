package com.cxliu.cnews.News.Widget;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cxliu.cnews.News.Bean.SimpleNewsBean;
import com.cxliu.cnews.R;
import com.cxliu.cnews.Utils.ImageDisplayUtils;
import com.cxliu.cnews.Utils.ViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lcx on 2016/7/12.
 */

public class NewsListAdapter extends BaseAdapter {

    private List<SimpleNewsBean> items = new ArrayList<SimpleNewsBean>();

    private Context mContext;

    public void setLists(List<SimpleNewsBean> beans) {
        if (beans != null) {
            items.clear();
            items.addAll(beans);
            notifyDataSetChanged();
        }
    }

    public NewsListAdapter(Context context){
        mContext=context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.adapter_news_list, null);
        }
        initView(convertView, position);
        return convertView;
    }

    private void initView(View convertView, int position){
        SimpleNewsBean beanItem=items.get(position);
        RelativeLayout root_view= ViewHolder.get(convertView, R.id.root_view);
        ImageView item_img=ViewHolder.get(convertView,R.id.item_img);
        TextView item_title=ViewHolder.get(convertView,R.id.item_title);
        TextView item_subtitle=ViewHolder.get(convertView,R.id.item_subtitle);
        ImageDisplayUtils.displayImage(mContext,beanItem.getImgsrc(),item_img);
        item_title.setText(beanItem.getTitle());
        item_subtitle.setText(beanItem.getDigest());
    }
}
