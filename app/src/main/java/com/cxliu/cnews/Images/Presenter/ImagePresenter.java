package com.cxliu.cnews.Images.Presenter;

/**
 * Created by liuchenxu on 2016/7/29.
 */
public interface ImagePresenter {
    public void loadImages();
}
