package com.cxliu.cnews.Images.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cxliu.cnews.Images.Bean.ImageBean;
import com.cxliu.cnews.R;
import com.cxliu.cnews.Utils.ImageDisplayUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by a on 2016/7/12.
 */

public class ImagesRecyclerListAdapter extends RecyclerView.Adapter<ImagesRecyclerListAdapter.ItemViewHolder> {

    private List<ImageBean> items = new ArrayList<ImageBean>();

    private Context mContext;

//    private onItemClickListener clickListener;

    public ImagesRecyclerListAdapter(Context context) {
        mContext = context;
    }

//    public void setClickListener(onItemClickListener listener){
//        clickListener=listener;
//    }

    public void setLists(List<ImageBean> beans) {
        if (beans != null) {
            items.clear();
            items.addAll(beans);
            notifyDataSetChanged();
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_images_list, parent, false);
        ItemViewHolder holder = new ItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        if (items == null || items.size() <= 0) {
            return;
        }
        ImageBean bean = items.get(position);
        if (bean == null) {
            return;
        }
        holder.mTitle.setText(bean.getTitle());
        ImageDisplayUtils.displayImage(mContext, bean.getThumburl(), holder.mImage);
    }

//    @Override
//    public void onBindViewHolder(RecycleViewViewHolder holder, final int position) {
//        ImageDisplayUtils.displayImage(mContext,items.get(position).getImgsrc(),holder.item_img);
//        holder.item_title.setText(items.get(position).getTitle());
//        holder.item_subtitle.setText(items.get(position).getDigest());
//    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView mTitle;
        public ImageView mImage;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.item_title);
            mImage = (ImageView) itemView.findViewById(R.id.item_image);
        }
    }
}
