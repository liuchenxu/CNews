package com.cxliu.cnews.Images.View;

import com.cxliu.cnews.Images.Bean.ImageBean;

import java.util.List;

/**
 * Created by liuchenxu on 2016/7/29.
 */
public interface IImageListView {
    void showProgress();
    void hideProgress();
    void loadImagesList();
    void setImagesList(List<ImageBean> iamges);
    void loadDataFail();
}
