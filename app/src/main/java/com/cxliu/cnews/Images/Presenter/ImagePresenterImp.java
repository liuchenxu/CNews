package com.cxliu.cnews.Images.Presenter;

import com.cxliu.cnews.Images.Api.ApiManager;
import com.cxliu.cnews.Images.Bean.ImageBean;
import com.cxliu.cnews.Images.View.IImageListView;

import java.util.List;

/**
 * Created by Administrator on 2016/7/29.
 */
public class ImagePresenterImp implements ImagePresenter {
    @Override
    public void loadImages() {
        ApiManager.getImagesList(new ApiManager.ImagesApiInterface() {
            @Override
            public void onSuccess(List<ImageBean> beans) {
                mIImageListView.setImagesList(beans);
            }

            @Override
            public void onFail() {
                mIImageListView.loadDataFail();
            }
        });
    }

    private IImageListView mIImageListView;

    public ImagePresenterImp(IImageListView iImageListView) {
        if (iImageListView != null) {
            mIImageListView = iImageListView;
        }
    }
}
