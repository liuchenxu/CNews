package com.cxliu.cnews.Images.widget;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cxliu.cnews.Images.Bean.ImageBean;
import com.cxliu.cnews.Images.Presenter.ImagePresenterImp;
import com.cxliu.cnews.Images.View.IImageListView;
import com.cxliu.cnews.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lcx on 2016/7/11.
 */

public class ImageListFragment extends Fragment implements IImageListView{

    private SwipeRefreshLayout swipe_refresh_layout;
    private RecyclerView id_recyclerview;
    private List<ImageBean> imagesList = new ArrayList<ImageBean>();
    private ImagePresenterImp mImagePresenter;
    private boolean isRefresh = true;
    private ImagesRecyclerListAdapter mImagesRecyclerListAdapter;

    public ImageListFragment() {
        mImagePresenter = new ImagePresenterImp(this);
    }

    public static ImageListFragment newInstance() {
        ImageListFragment fragment = new ImageListFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImagePresenter = new ImagePresenterImp(this);
    }


    @Override
    public void showProgress() {
        swipe_refresh_layout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipe_refresh_layout.setRefreshing(false);
    }

    @Override
    public void loadImagesList() {
        mImagePresenter.loadImages();
    }

    @Override
    public void setImagesList(List<ImageBean> images) {
        hideProgress();
        mImagesRecyclerListAdapter.setLists(images);
    }


    @Override
    public void loadDataFail() {
        hideProgress();
        Toast.makeText(getActivity(), "加载失败", Toast.LENGTH_SHORT).show();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_list, container, false);
        swipe_refresh_layout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
//        news_list_view = (ListView) view.findViewById(news_list_view);
        id_recyclerview = (RecyclerView) view.findViewById(R.id.id_recyclerview);
        //设置布局管理器
        id_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        //设置Item增加、移除动画
        id_recyclerview.setItemAnimator(new DefaultItemAnimator());
        swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isRefresh = true;
//                Page_Index = 0;
//                loadNewsList();
                loadImagesList();
            }
        });
        loadImagesList();
        mImagesRecyclerListAdapter = new ImagesRecyclerListAdapter(getActivity());
//        mNewsRecyclerListAdapter.setClickListener(new NewsRecyclerListAdapter.onItemClickListener() {
//            @Override
//            public void onClick(SimpleNewsBean news) {
//                Intent intent=new Intent(getActivity(),NewsDetailActivity.class);
//                intent.putExtra("bean",news);
//                getActivity().startActivity(intent);
//            }
//        });
        id_recyclerview.setAdapter(mImagesRecyclerListAdapter);
        return view;
    }
}
