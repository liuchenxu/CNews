package com.cxliu.cnews.Utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.cxliu.cnews.R;

/**
 * Created by lcx on 2016/7/12.
 */

public class ImageDisplayUtils {
    public static void displayImage(Context mContext, String url, ImageView img) {
        if (img != null) {
            Glide.with(mContext).load(url).placeholder(R.drawable.ic_image_loading).error(R.drawable.ic_image_loadfail).crossFade().into(img);
        }
    }
}
