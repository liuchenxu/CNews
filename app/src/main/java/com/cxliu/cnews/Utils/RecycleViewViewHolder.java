package com.cxliu.cnews.Utils;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cxliu.cnews.News.Bean.SimpleNewsBean;
import com.cxliu.cnews.News.Widget.NewsRecyclerListAdapter;
import com.cxliu.cnews.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lcx on 2016/7/12.
 */

public class RecycleViewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView item_img;
    public TextView item_title;
    public TextView item_subtitle;
    public RelativeLayout item_root;
    private NewsRecyclerListAdapter.onItemClickListener mListener;
    private List<SimpleNewsBean> beans=new ArrayList<SimpleNewsBean>();

    public RecycleViewViewHolder(View itemView, NewsRecyclerListAdapter.onItemClickListener listener,List<SimpleNewsBean> items) {
        super(itemView);
        item_img = (ImageView) itemView.findViewById(R.id.item_img);
        item_title = (TextView) itemView.findViewById(R.id.item_title);
        item_subtitle = (TextView) itemView.findViewById(R.id.item_subtitle);
        item_root=(RelativeLayout) itemView.findViewById(R.id.sub_root);
        item_root.setOnClickListener(this);
        mListener = listener;
        beans.clear();
        beans.addAll(items);
    }

    @Override
    public void onClick(View view) {
        if (mListener != null) {
            mListener.onClick(beans.get(getPosition()));
        }
    }
}
